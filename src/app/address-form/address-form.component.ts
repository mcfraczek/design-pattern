import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormHandler} from '../model/form-handler';
import {FormControl, FormGroup} from '@angular/forms';
import {GeolocationItem} from '../model/geolocation-item';
import {ObjectToSendToBackend} from '../model/object-to-send-to-backend';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {

  @Output()
  public submit: EventEmitter<ObjectToSendToBackend> = new EventEmitter<ObjectToSendToBackend>();

  private readonly country: FormHandler = new FormHandler(undefined);
  private readonly province: FormHandler = new FormHandler(undefined);
  private readonly county: FormHandler = new FormHandler(undefined);
  private readonly commune: FormHandler = new FormHandler(undefined);
  private readonly locality: FormHandler = new FormHandler(undefined);
  private readonly street: FormHandler = new FormHandler(undefined);

  public form: FormGroup = new FormGroup({
    country: new FormControl(undefined),
    province: new FormControl(undefined),
    county: new FormControl(undefined),
    commune: new FormControl(undefined),
    locality: new FormControl(undefined),
    street: new FormControl(undefined),
  });

  // this values would typically come from the backend
  countries: GeolocationItem [] = [
    {
      id: '1',
      name: 'country A'
    },
    {
      id: '2',
      name: 'country B'
    }
  ];
  provinces: GeolocationItem [] = [
    {
      id: '1',
      name: 'province A'
    },
    {
      id: '2',
      name: 'province B'
    }
  ];
  counties: GeolocationItem [] = [
    {
      id: '1',
      name: 'county A'
    },
    {
      id: '2',
      name: 'county B'
    }
  ];
  communes: GeolocationItem [] = [
    {
      id: 'commune 1',
      name: 'commune A'
    },
    {
      id: 'commune 2',
      name: 'commune B'
    }
  ];
  localities: GeolocationItem [] = [
    {
      id: '1',
      name: 'locality A'
    },
    {
      id: '2',
      name: 'locality B'
    }
  ];
  streets: GeolocationItem [] = [
    {
      id: '1',
      name: 'street A'
    },
    {
      id: '2',
      name: 'street B'
    }
  ];

  public ngOnInit(): void {
    this.country
      .setNext(this.province)
      .setNext(this.county)
      .setNext(this.commune)
      .setNext(this.locality)
      .setNext(this.street);

    this.form.setControl('country', this.country);
    this.form.setControl('province', this.province);
    this.form.setControl('county', this.county);
    this.form.setControl('commune', this.commune);
    this.form.setControl('locality', this.locality);
    this.form.setControl('street', this.street);
  }

  public onCountryChange(): void {
    this.country.handleAll();
  }

  public onProvinceChange(): void {
    this.province.handleAll();
  }

  public onCountyChange(): void {
    this.county.handleAll();
  }

  public onCommuneChange(): void {
    this.commune.handleAll();
  }

  public onLocalityChange(): void {
    this.locality.handleAll();
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this.submit.emit(this.form.value);
    }
  }
}
