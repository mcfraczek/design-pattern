import {GeolocationItem} from './model/geolocation-item';

export const countries: GeolocationItem [] = [
  {
    id: '1',
    name: 'country A'
  },
  {
    id: '2',
    name: 'country B'
  }
];
export const provinces: GeolocationItem [] = [
  {
    id: '1',
    name: 'province A'
  },
  {
    id: '2',
    name: 'province B'
  }
];
export const counties: GeolocationItem [] = [
  {
    id: '1',
    name: 'county A'
  },
  {
    id: '2',
    name: 'county B'
  }
];
export const communes: GeolocationItem [] = [
  {
    id: 'commune 1',
    name: 'commune A'
  },
  {
    id: 'commune 2',
    name: 'commune B'
  }
];
export const localities: GeolocationItem [] = [
  {
    id: '1',
    name: 'locality A'
  },
  {
    id: '2',
    name: 'locality B'
  }
];
export const streets: GeolocationItem [] = [
  {
    id: '1',
    name: 'street A'
  },
  {
    id: '2',
    name: 'street B'
  }
];
