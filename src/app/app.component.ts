import {Component} from '@angular/core';
import {ObjectToSendToBackend} from './model/object-to-send-to-backend';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'chain-of-responsibility';

  public onSubmit(object: ObjectToSendToBackend): void {
    console.log(object);
  }

}
