import {GeolocationItem} from './geolocation-item';

export interface ObjectToSendToBackend {
  country?: GeolocationItem;
  province?: GeolocationItem;
  county?: GeolocationItem;
  commune?: GeolocationItem;
  locality?: GeolocationItem;
  street?: GeolocationItem;
}
