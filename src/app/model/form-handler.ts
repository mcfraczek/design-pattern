import { FormControl } from '@angular/forms';

/*Chain of responsibility*/

interface Handler {
  setNext(handler: Handler): Handler;

  handleAll(): void;
}

export class FormHandler extends FormControl implements Handler {
  // @ts-ignore
  private nextHandler: FormHandler = undefined;

  public setNext(handler: FormHandler): FormHandler {
    this.nextHandler = handler;

    return handler;
  }

  public handleAll(): void {
    this.handleNext();
  }

  private clearValue(): void {
    if (!this.pristine) {
      this.markAsPristine();
    }
    if (!this.untouched) {
      this.markAsUntouched();
    }
    this.setValue(undefined);
  }

  private handleNext(): void {
    if (this.nextHandler) {
      this.nextHandler.clearValue();
      this.nextHandler.handleNext();
    }
  }

}
